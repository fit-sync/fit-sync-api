package com.pc.fitsyncapi.controller;

import com.pc.fitsyncapi.model.dto.AuthenticationRequestDto;
import com.pc.fitsyncapi.model.dto.JwtDto;
import com.pc.fitsyncapi.service.JwtService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/login")
@RequiredArgsConstructor
public class LoginController {

  private final JwtService jwtService;

  /**
   * Authenticates a user.
   *
   * @param authRequestDto The authentication details of the user.
   * @return The generated JWT as a {@link JwtDto} object, alongside the OK HTTP status.
   */
  @PostMapping
  public ResponseEntity<JwtDto> login(@RequestBody @Valid AuthenticationRequestDto authRequestDto) {
    return ResponseEntity.status(HttpStatus.OK).body(jwtService.authenticate(authRequestDto));
  }
}
