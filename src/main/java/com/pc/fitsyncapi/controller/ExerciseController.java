package com.pc.fitsyncapi.controller;


import com.pc.fitsyncapi.controller.customResponse.PaginatedExerciseResponse;
import com.pc.fitsyncapi.service.ExerciseService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/exercises")
@RequiredArgsConstructor
public class ExerciseController {

  private final ExerciseService exerciseService;

  /**
   * Retrieves a paginated list of exercises based on the provided filters.
   *
   * @param categories   A list of exercise categories to filter by. Can be empty or null.
   * @param muscleGroups A list of muscle groups to filter by. Can be empty or null.
   * @param name         A partial or full exercise name to filter by. Can be empty or null.
   * @param pageable     Pagination information, including page number, size, and sorting.
   * @return ResponseEntity containing a PaginatedExerciseResponse with filtered exercises. Returns HttpStatus.OK if
   * successful.
   */
  @GetMapping
  public ResponseEntity<PaginatedExerciseResponse> getFilteredPaginatedExercises(
      @RequestParam(required = false) List<String> categories,
      @RequestParam(required = false) List<String> muscleGroups,
      @RequestParam(required = false) String name,
      Pageable pageable) {
    return new ResponseEntity<>(exerciseService.getFilteredExerciseDtos(categories, muscleGroups, name, pageable),
        HttpStatus.OK);
  }

}
