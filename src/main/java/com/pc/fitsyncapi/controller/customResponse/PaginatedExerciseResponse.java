package com.pc.fitsyncapi.controller.customResponse;

import com.pc.fitsyncapi.model.dto.ExerciseDto;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaginatedExerciseResponse {

  private List<ExerciseDto> exercises;
  private Long numberOfExercises;
  private Integer numberOfPages;

}
