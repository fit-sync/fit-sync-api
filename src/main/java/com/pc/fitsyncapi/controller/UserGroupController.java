package com.pc.fitsyncapi.controller;

import com.pc.fitsyncapi.model.dto.UserGroupDto;
import com.pc.fitsyncapi.service.UserGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping(value = "/api/user-groups")
@RequiredArgsConstructor
public class UserGroupController {

    private final UserGroupService userGroupService;

    @PostMapping
    public ResponseEntity<UserGroupDto> createUserGroup(@Valid @RequestBody UserGroupDto userGroupDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userGroupService.createUserGroup(userGroupDto));
    }

    @GetMapping
    public ResponseEntity<List<UserGroupDto>> getUserGroups(Authentication authentication) {
        return ResponseEntity.status(HttpStatus.OK).body(userGroupService.getUserGroups(authentication.getName()));
    }

}
