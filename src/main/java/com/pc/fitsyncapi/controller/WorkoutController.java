package com.pc.fitsyncapi.controller;

import com.pc.fitsyncapi.model.dto.AddWorkoutDto;
import com.pc.fitsyncapi.model.dto.WorkoutDto;
import com.pc.fitsyncapi.service.WorkoutService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/workouts")
public class WorkoutController {

  private final WorkoutService workoutService;

  @PostMapping
  public ResponseEntity<WorkoutDto> addWorkout(Authentication authentication,
      @Valid @RequestBody AddWorkoutDto addWorkoutDto) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(workoutService.addWorkout(authentication.getName(), addWorkoutDto));
  }

  @GetMapping
  public ResponseEntity<List<WorkoutDto>> getWorkouts(@RequestParam(required = false) List<String> userEmails) {
    return ResponseEntity.status(HttpStatus.OK).body(workoutService.getFilteredWorkouts(userEmails));
  }

}
