package com.pc.fitsyncapi.controller;

import com.pc.fitsyncapi.service.UserAccountService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/users")
@RequiredArgsConstructor
public class UserAccountController {

  private final UserAccountService userAccountService;

  @GetMapping(value = "/emails")
  public ResponseEntity<List<String>> getAllUserEmails(){
    return ResponseEntity.ok(userAccountService.getAllUserEmails());
  }
}
