package com.pc.fitsyncapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainingSetDto {

  private Integer weight;

  private Integer reps;

}
