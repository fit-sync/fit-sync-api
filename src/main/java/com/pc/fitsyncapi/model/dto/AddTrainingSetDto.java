package com.pc.fitsyncapi.model.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddTrainingSetDto {

  @NotNull(message = "Weight is missing")
  private Integer weight;

  @NotNull(message = "Reps are missing")
  private Integer reps;

}
