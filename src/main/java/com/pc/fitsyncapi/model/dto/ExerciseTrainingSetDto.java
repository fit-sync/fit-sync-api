package com.pc.fitsyncapi.model.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseTrainingSetDto {

  private String name;

  private String description;

  private String category;

  private List<MuscleGroupDto> muscleGroupDtos;

  private List<TrainingSetDto> trainingSetDtos;

}
