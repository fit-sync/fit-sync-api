package com.pc.fitsyncapi.model.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthenticationRequestDto {

  @NotNull(message = "Email is missing")
  private String email;

  @NotNull(message = "Password is missing")
  private String password;
}
