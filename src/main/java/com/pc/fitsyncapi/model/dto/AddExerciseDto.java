package com.pc.fitsyncapi.model.dto;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddExerciseDto {

  @NotNull(message = "Exercise is missing")
  private Long exerciseId;

  @NotNull(message = "Training set(s) missing")
  private List<AddTrainingSetDto> trainingSets;

}
