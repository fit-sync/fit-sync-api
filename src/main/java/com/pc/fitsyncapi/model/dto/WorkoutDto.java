package com.pc.fitsyncapi.model.dto;

import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkoutDto {

  private String name;

  private LocalDateTime startDateTime;

  private LocalDateTime endDateTime;

  private List<ExerciseTrainingSetDto> exercises;

}
