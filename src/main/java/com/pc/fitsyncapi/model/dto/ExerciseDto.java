package com.pc.fitsyncapi.model.dto;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExerciseDto {

  private Long id;

  private String name;

  private String description;

  private String category;

  private List<String> muscleGroups;

}