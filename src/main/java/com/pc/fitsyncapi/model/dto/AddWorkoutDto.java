package com.pc.fitsyncapi.model.dto;

import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddWorkoutDto {

  @NotNull(message = "Workout name is missing")
  private String name;

  @NotNull(message = "Start date time is missing")
  private LocalDateTime startDateTime;

  @NotNull(message = "End date time is missing")
  private LocalDateTime endDateTime;

  @NotNull(message = "Exercise(s) missing")
  private List<AddExerciseDto> exercises;

}
