package com.pc.fitsyncapi.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "muscle_group")
@NoArgsConstructor
@Getter
@Setter
public class MuscleGroup {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String name;

  @ManyToMany
  @JoinTable(
      name = "exercise_muscle_group",
      joinColumns = {@JoinColumn(name = "muscle_group_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "exercise_id", referencedColumnName = "id")}
  )
  private List<Exercise> exercises;

}
