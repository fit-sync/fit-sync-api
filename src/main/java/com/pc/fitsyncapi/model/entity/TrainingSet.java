package com.pc.fitsyncapi.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "training_set")
@NoArgsConstructor
@Getter
@Setter
public class TrainingSet {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private Integer weight;

  @Column
  private Integer reps;

  @ManyToOne
  @JoinColumn(name = "workout_exercise_id")
  private WorkoutExercise workoutExercise;

}
