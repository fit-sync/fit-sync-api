package com.pc.fitsyncapi.model.entity;

import jakarta.persistence.*;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_account")
@NoArgsConstructor
@Getter
@Setter
public class UserAccount {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private String email;

  @Column
  private String password;

  @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Workout> workouts;

  @ManyToMany
  @JoinTable(
          name = "user_account_user_group",
          joinColumns = {@JoinColumn(name = "user_account_id", referencedColumnName = "id")},
          inverseJoinColumns = {@JoinColumn(name = "user_group_id", referencedColumnName = "id")}
  )
  private List<UserGroup> userGroups;

}
