package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.model.dto.UserGroupDto;
import com.pc.fitsyncapi.model.entity.UserAccount;
import com.pc.fitsyncapi.model.entity.UserGroup;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface UserGroupMapper {

    List<UserGroupDto> toUserGroupDtos(List<UserGroup> userGroups);

    @Mapping(source = "userAccounts", target = "userEmails", qualifiedByName = "mapToUserEmails")
    UserGroupDto toUserGroupDto(UserGroup userGroup);

    @Named("mapToUserEmails")
    default List<String> mapUserAccounts(List<UserAccount> userAccounts) {
        return userAccounts.stream().map(UserAccount::getEmail).collect(Collectors.toList());
    }

}
