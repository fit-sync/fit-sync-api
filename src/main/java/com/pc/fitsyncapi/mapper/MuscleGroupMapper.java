package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.model.dto.MuscleGroupDto;
import com.pc.fitsyncapi.model.entity.MuscleGroup;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface MuscleGroupMapper {

  List<MuscleGroupDto> toListOfMuscleGroupDto(List<MuscleGroup> muscleGroupList);

}
