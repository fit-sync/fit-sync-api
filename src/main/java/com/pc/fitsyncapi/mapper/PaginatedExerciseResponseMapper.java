package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.controller.customResponse.PaginatedExerciseResponse;
import com.pc.fitsyncapi.model.entity.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

@Mapper(uses = ExerciseMapper.class)
public interface PaginatedExerciseResponseMapper {

  @Mapping(source = "content", target = "exercises")
  @Mapping(source = "totalElements", target = "numberOfExercises")
  @Mapping(source = "totalPages", target = "numberOfPages")
  PaginatedExerciseResponse toPaginatedExerciseResponse(Page<Exercise> exercises);

}
