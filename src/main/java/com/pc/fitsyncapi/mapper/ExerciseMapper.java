package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.model.dto.ExerciseDto;
import com.pc.fitsyncapi.model.dto.ExerciseTrainingSetDto;
import com.pc.fitsyncapi.model.entity.Exercise;
import com.pc.fitsyncapi.model.entity.MuscleGroup;
import com.pc.fitsyncapi.model.entity.WorkoutExercise;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(uses = {MuscleGroupMapper.class, TrainingSetMapper.class})
public interface ExerciseMapper {

  @Mapping(source = "workoutExercise.exercise.name", target = "name")
  @Mapping(source = "workoutExercise.exercise.description", target = "description")
  @Mapping(source = "workoutExercise.exercise.category.name", target = "category")
  @Mapping(source = "workoutExercise.exercise.muscleGroups", target = "muscleGroupDtos")
  @Mapping(source = "workoutExercise.sets", target = "trainingSetDtos")
  ExerciseTrainingSetDto toExerciseTrainingDto(WorkoutExercise workoutExercise);

  List<ExerciseTrainingSetDto> toListOfExerciseTrainingDto(List<WorkoutExercise> workoutExercises);

  @Mapping(source = "category.name", target = "category")
  @Mapping(source = "muscleGroups", target = "muscleGroups", qualifiedByName = "mapToMuscleGroupNames")
  ExerciseDto toExerciseDto(Exercise exercise);

  @Named("mapToMuscleGroupNames")
  default List<String> mapMuscleGroups(List<MuscleGroup> muscleGroup) {
    return muscleGroup.stream().map(MuscleGroup::getName).collect(Collectors.toList());
  }
}
