package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.model.dto.WorkoutDto;
import com.pc.fitsyncapi.model.entity.Workout;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(uses = {ExerciseMapper.class})
public interface WorkoutMapper {

  WorkoutDto toWorkoutDto(Workout workout);

  List<WorkoutDto> toListOfWorkoutDto(List<Workout> workout);

}
