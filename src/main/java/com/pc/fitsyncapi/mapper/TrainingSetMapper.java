package com.pc.fitsyncapi.mapper;

import com.pc.fitsyncapi.model.dto.AddTrainingSetDto;
import com.pc.fitsyncapi.model.dto.TrainingSetDto;
import com.pc.fitsyncapi.model.entity.TrainingSet;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface TrainingSetMapper {

  TrainingSetDto toTrainingSetDto(TrainingSet trainingSet);

  List<TrainingSet> toListOfTrainingSet(List<AddTrainingSetDto> addTrainingSetDtos);

}
