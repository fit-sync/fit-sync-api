package com.pc.fitsyncapi.service;

import com.pc.fitsyncapi.controller.customResponse.PaginatedExerciseResponse;
import com.pc.fitsyncapi.mapper.PaginatedExerciseResponseMapper;
import com.pc.fitsyncapi.model.dto.ExerciseDto;
import com.pc.fitsyncapi.model.entity.Exercise;
import com.pc.fitsyncapi.repository.ExerciseRepository;
import jakarta.annotation.Nullable;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class ExerciseService {

  private final ExerciseRepository exerciseRepository;
  private final PaginatedExerciseResponseMapper paginatedExerciseResponseMapper;

  /**
   * Retrieves a paginated list of exercises based on the provided filters.
   *
   * @param categories   A list of exercise categories to filter by. Can be empty or null.
   * @param muscleGroups A list of muscle groups to filter by. Can be empty or null.
   * @param name         A partial or full exercise name to filter by. Can be empty or null.
   * @param pageable     Pagination information, including page number, size, and sorting.
   * @return PaginatedExerciseResponse containing filtered exercises.
   */
  public PaginatedExerciseResponse getFilteredExerciseDtos(@Nullable List<String> categories,
      @Nullable List<String> muscleGroups, @Nullable String name, Pageable pageable) {
    return paginatedExerciseResponseMapper.toPaginatedExerciseResponse(
        exerciseRepository.findAll(createExerciseSpecification(categories, muscleGroups, name), pageable));
  }

  private Specification<Exercise> createExerciseSpecification(@Nullable List<String> categories,
      @Nullable List<String> muscleGroups, @Nullable String name) {
    return (root, query, criteriaBuilder) -> {
      List<Predicate> predicates = new ArrayList<>();

      if (!CollectionUtils.isEmpty(categories)) {
        predicates.add(root.join("category", JoinType.INNER).get("name").in(categories));
      }
      if (!CollectionUtils.isEmpty(muscleGroups)) {
        predicates.add(root.join("muscleGroups", JoinType.INNER).get("name").in(muscleGroups));
      }
      if (StringUtils.hasText(name)) {
        String searchTerm = "%" + name.toLowerCase() + "%";
        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), searchTerm));
      }

      return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    };
  }

}
