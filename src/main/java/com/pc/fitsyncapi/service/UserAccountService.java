package com.pc.fitsyncapi.service;

import com.pc.fitsyncapi.repository.UserAccountRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserAccountService {

  private final UserAccountRepository userAccountRepository;

  public List<String> getAllUserEmails(){
    return userAccountRepository.findAllEmails();
  }
}
