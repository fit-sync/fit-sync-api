package com.pc.fitsyncapi.service;

import static com.pc.fitsyncapi.exception.ErrorMessages.ENTITY_NOT_FOUND_MSG;

import com.pc.fitsyncapi.exception.EntityNotFoundException;
import com.pc.fitsyncapi.mapper.TrainingSetMapper;
import com.pc.fitsyncapi.mapper.WorkoutMapper;
import com.pc.fitsyncapi.model.dto.AddWorkoutDto;
import com.pc.fitsyncapi.model.dto.WorkoutDto;
import com.pc.fitsyncapi.model.entity.Exercise;
import com.pc.fitsyncapi.model.entity.TrainingSet;
import com.pc.fitsyncapi.model.entity.UserAccount;
import com.pc.fitsyncapi.model.entity.Workout;
import com.pc.fitsyncapi.model.entity.WorkoutExercise;
import com.pc.fitsyncapi.repository.ExerciseRepository;
import com.pc.fitsyncapi.repository.TrainingSetRepository;
import com.pc.fitsyncapi.repository.UserAccountRepository;
import com.pc.fitsyncapi.repository.WorkoutExerciseRepository;
import com.pc.fitsyncapi.repository.WorkoutRepository;
import jakarta.annotation.Nullable;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@RequiredArgsConstructor
public class WorkoutService {

  private final ExerciseRepository exerciseRepository;

  private final TrainingSetRepository trainingSetRepository;

  private final UserAccountRepository userAccountRepository;

  private final WorkoutExerciseRepository workoutExerciseRepository;

  private final WorkoutRepository workoutRepository;

  private final TrainingSetMapper trainingSetMapper;

  private final WorkoutMapper workoutMapper;

  @Transactional
  public WorkoutDto addWorkout(String email, AddWorkoutDto addWorkoutDto) {
    UserAccount userAccount = userAccountRepository.findByEmail(email).orElseThrow(
        () -> new EntityNotFoundException(ENTITY_NOT_FOUND_MSG.formatted(UserAccount.class.getSimpleName())));

    Workout workout = Workout.builder()
        .userAccount(userAccount)
        .name(addWorkoutDto.getName())
        .startDateTime(addWorkoutDto.getStartDateTime())
        .endDateTime(addWorkoutDto.getEndDateTime())
        .build();

    Workout savedWorkout = workoutRepository.save(workout);

    List<WorkoutExercise> workoutExercises = new ArrayList<>();
    addWorkoutDto.getExercises().forEach(exerciseDto -> {
      Exercise exercise = exerciseRepository.findById(exerciseDto.getExerciseId()).orElseThrow(
          () -> new EntityNotFoundException(ENTITY_NOT_FOUND_MSG.formatted(Exercise.class.getSimpleName())));

      WorkoutExercise workoutExercise = WorkoutExercise.builder()
          .workout(savedWorkout)
          .exercise(exercise)
          .build();
      WorkoutExercise savedWorkoutExercise = workoutExerciseRepository.save(workoutExercise);

      List<TrainingSet> trainingSets = trainingSetMapper.toListOfTrainingSet(exerciseDto.getTrainingSets());
      trainingSets.forEach(trainingSet -> {
        trainingSet.setWorkoutExercise(savedWorkoutExercise);
        trainingSetRepository.save(trainingSet);
      });

      workoutExercise.setSets(trainingSets);
      workoutExercises.add(workoutExercise);
    });

    savedWorkout.setExercises(workoutExercises);

    return workoutMapper.toWorkoutDto(savedWorkout);
  }

  public List<WorkoutDto> getFilteredWorkouts(@Nullable List<String> userEmails) {

    return workoutMapper.toListOfWorkoutDto(workoutRepository.findAll(createWorkoutSpecification(userEmails)));
  }

  private Specification<Workout> createWorkoutSpecification(@Nullable List<String> userEmails) {
    return (root, query, criteriaBuilder) -> {
      List<Predicate> predicates = new ArrayList<>();
      if(!CollectionUtils.isEmpty(userEmails)) {
        Join<Workout, UserAccount> userAccountJoin = root.join("userAccount");
        predicates.add(userAccountJoin.get("email").in(userEmails));
      }
      return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    };
  }

}
