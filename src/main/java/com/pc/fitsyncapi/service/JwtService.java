package com.pc.fitsyncapi.service;


import static com.pc.fitsyncapi.constants.SecurityConstants.ID_CLAIM_NAME;
import static com.pc.fitsyncapi.constants.SecurityConstants.JWT_KEY;

import com.pc.fitsyncapi.model.dto.AuthenticationRequestDto;
import com.pc.fitsyncapi.model.dto.JwtDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import javax.crypto.SecretKey;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtService {

  private static final Long JWT_EXPIRATION_TIME_IN_MS = 24 * 60 * 60 * 1000L;

  private final AuthenticationManager authManager;

  /**
   * Authenticates a user, by generating a JWT for an authentication.
   *
   * @param authRequestDto The authentication details to generate the JWT for.
   * @return The generated JWT as a {@link JwtDto} object.
   */
  public JwtDto authenticate(AuthenticationRequestDto authRequestDto) {
    Authentication authentication = authManager.authenticate(
        new UsernamePasswordAuthenticationToken(authRequestDto.getEmail(), authRequestDto.getPassword()));

    return JwtDto.builder().token(generateJwt(authentication)).build();
  }

  private String generateJwt(Authentication authentication) {
    SecretKey key = Keys.hmacShaKeyFor(JWT_KEY.getBytes(StandardCharsets.UTF_8));

    return Jwts.builder()
        .setIssuer("FitSync")
        .setSubject("JWT")
        .claim(ID_CLAIM_NAME, authentication.getName())
        .setIssuedAt(new Date())
        .setExpiration(new Date((new Date()).getTime() + JWT_EXPIRATION_TIME_IN_MS))
        .signWith(key).compact();
  }
}
