package com.pc.fitsyncapi.service;

import com.pc.fitsyncapi.exception.EntityNotFoundException;
import com.pc.fitsyncapi.mapper.UserGroupMapper;
import com.pc.fitsyncapi.model.dto.UserGroupDto;
import com.pc.fitsyncapi.model.entity.UserAccount;
import com.pc.fitsyncapi.model.entity.UserGroup;
import com.pc.fitsyncapi.repository.UserAccountRepository;
import com.pc.fitsyncapi.repository.UserGroupRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.pc.fitsyncapi.exception.ErrorMessages.ENTITY_NOT_FOUND_MSG;

@Service
@RequiredArgsConstructor
public class UserGroupService {

    private final UserGroupMapper userGroupMapper;

    private final UserAccountRepository userAccountRepository;

    private final UserGroupRepository userGroupRepository;

    @Transactional
    public UserGroupDto createUserGroup(UserGroupDto userGroupDto) {
        UserGroup userGroup = new UserGroup();
        userGroup.setName(userGroupDto.getName());
        userGroupRepository.save(userGroup);

        List<UserAccount> userAccounts = new ArrayList<>();
        for (String email : userGroupDto.getUserEmails()) {
            UserAccount userAccount = userAccountRepository.findByEmail(email).orElseThrow(
                    () -> new EntityNotFoundException(ENTITY_NOT_FOUND_MSG.formatted(UserAccount.class.getSimpleName())));
            userAccount.getUserGroups().add(userGroup);
            userAccounts.add(userAccount);
        }

        userGroup.setUserAccounts(userAccounts);

        return userGroupDto;
    }

    public List<UserGroupDto> getUserGroups(String email) {
        UserAccount userAccount = userAccountRepository.findByEmail(email).orElseThrow(
                () -> new EntityNotFoundException(ENTITY_NOT_FOUND_MSG.formatted(UserAccount.class.getSimpleName())));

        return userGroupMapper.toUserGroupDtos(userGroupRepository.findAllByUserAccountsContaining(userAccount));
    }

}
