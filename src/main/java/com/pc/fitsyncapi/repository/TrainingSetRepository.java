package com.pc.fitsyncapi.repository;

import com.pc.fitsyncapi.model.entity.TrainingSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingSetRepository extends JpaRepository<TrainingSet, Long> {

}
