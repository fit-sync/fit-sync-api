package com.pc.fitsyncapi.repository;

import com.pc.fitsyncapi.model.entity.UserAccount;
import com.pc.fitsyncapi.model.entity.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

    List<UserGroup> findAllByUserAccountsContaining(UserAccount userAccount);

}
