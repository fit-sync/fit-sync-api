package com.pc.fitsyncapi.repository;

import com.pc.fitsyncapi.model.entity.Exercise;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {

  Page<Exercise> findAll(Specification<Exercise> spec, Pageable pageable);

}
