package com.pc.fitsyncapi.repository;

import com.pc.fitsyncapi.model.entity.Workout;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Long> {

  List<Workout> findAll(Specification<Workout> spec);
}
