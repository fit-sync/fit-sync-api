package com.pc.fitsyncapi.repository;

import com.pc.fitsyncapi.model.entity.UserAccount;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

  Optional<UserAccount> findByEmail(String email);

  @Query("SELECT u.email FROM UserAccount u")
  List<String> findAllEmails();

}
