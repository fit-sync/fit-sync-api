package com.pc.fitsyncapi.constants;

public class SecurityConstants {

  public static final String AUTH_HEADER = "Authorization";

  public static final String JWT_KEY = "jxgEQeXHuPq8VdbyYFNkANdudQ53YUn4";

  public static final String ID_CLAIM_NAME = "email";
}
