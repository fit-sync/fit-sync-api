package com.pc.fitsyncapi.config.filter;

import static com.pc.fitsyncapi.constants.SecurityConstants.AUTH_HEADER;
import static com.pc.fitsyncapi.constants.SecurityConstants.ID_CLAIM_NAME;
import static com.pc.fitsyncapi.constants.SecurityConstants.JWT_KEY;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.crypto.SecretKey;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class JwtValidatorFilter extends OncePerRequestFilter {

  private static final String INVALID_TOKEN_MSG = "Invalid token";

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws IOException {
    String jwt = request.getHeader(AUTH_HEADER);

    if (jwt != null) {
      String jwtValue = jwt.substring(7);
      SecretKey key = Keys.hmacShaKeyFor(JWT_KEY.getBytes(StandardCharsets.UTF_8));

      try {
        Claims claims = Jwts.parserBuilder()
            .setSigningKey(key)
            .build()
            .parseClaimsJws(jwtValue)
            .getBody();

        String email = String.valueOf(claims.get(ID_CLAIM_NAME));
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(email, null, null);
        SecurityContextHolder.getContext().setAuthentication(auth);

        filterChain.doFilter(request, response);
      } catch (Exception ex) {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), INVALID_TOKEN_MSG);
      }
    } else {
      response.sendError(HttpStatus.UNAUTHORIZED.value(), INVALID_TOKEN_MSG);
    }
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
    String requestPath = request.getServletPath();
    return requestPath.equals("/api/login");
  }
}
