package com.pc.fitsyncapi.config;

import com.pc.fitsyncapi.model.entity.UserAccount;
import com.pc.fitsyncapi.repository.UserAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider {

  private static final String BAD_CREDENTIALS_MSG = "Bad credentials";

  private final PasswordEncoder passwordEncoder;

  private final UserAccountRepository userAccountRepository;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String email = authentication.getName();
    String password = authentication.getCredentials().toString();

    UserAccount userAccount = userAccountRepository.findByEmail(email)
        .orElseThrow(() -> new BadCredentialsException(BAD_CREDENTIALS_MSG));

    if (!passwordEncoder.matches(password, userAccount.getPassword())) {
      throw new BadCredentialsException(BAD_CREDENTIALS_MSG);
    }

    return new UsernamePasswordAuthenticationToken(email, password, null);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }
}
