-- Insert mock data into exercise_category table
INSERT INTO exercise_category (name) VALUES ('Barbell');
INSERT INTO exercise_category (name) VALUES ('Dumbell');
INSERT INTO exercise_category (name) VALUES ('Machine');
INSERT INTO exercise_category (name) VALUES ('Bodywheight');

-- Insert mock data into muscle_group table
INSERT INTO muscle_group (name) VALUES ('Core');
INSERT INTO muscle_group (name) VALUES ('Arms');
INSERT INTO muscle_group (name) VALUES ('Back');
INSERT INTO muscle_group (name) VALUES ('Chest');
INSERT INTO muscle_group (name) VALUES ('Legs');
INSERT INTO muscle_group (name) VALUES ('Shoulders');
INSERT INTO muscle_group (name) VALUES ('Full body');

-- Insert mock data into exercise table
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Bench Press', '', 1);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Bent Over Row', '', 1);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Bicep Curl', '', 1);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Deadlift', '', 1);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Bench Press', '', 2);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Bent Over One Arm Row', '', 2);
INSERT INTO exercise (name, description, exercise_category_id) VALUES ('Chest Fly', '', 2);


-- Insert mock data into exercise_muscle_group table (assuming many-to-many relationship)
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (1, 4);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (2, 3);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (3, 2);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (4, 3);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (5, 4);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (6, 3);
INSERT INTO exercise_muscle_group (exercise_id, muscle_group_id) VALUES (7, 4);
